package calendar;

/**
* This class is used to house the main method
*/
public class Main{
    /**
    * This is the main method
    * @param args the command line arguments
    */
    public static void main(String[] args){
        DateGUI dateGUI = new DateGUI();
        Date date = dateGUI.setVDate();
        System.out.println(date.getDate());
    }
}