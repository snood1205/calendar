package calendar;

import java.awt.GridLayout;
import java.awt.event.*;
import javax.swing.*;

/**
* This field holds the 
* graphical representation
* of the <code>Date</code>
* class. 
*/
public class DateGUI{
    /* This field stores the date */
    private Date date;

    /**
    * This is the default
    * constructor which sets
    * the date to <code>null</code> 
    */
    public DateGUI() {
        this.date = null;
    }

    /**
    * This constructor sets
    * the date.
    * @param date the date to set
    */
    public DateGUI(Date date) {
        this.date = date;
    }

    /**
    * Gets the date
    * @return the date
    */
    public Date getDate() {
        return date;
    }

    /**
    * Sets the date
    * @param date the date to set
    */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
    * This method sets
    * the date visually.
    * It also returns
    * the date it sets.
    * @return the date set 
    */
    public Date setVDate() {
        final Date date = new Date();
        JPanel panel = new JPanel(new GridLayout(4,2));
        JFrame frame = new JFrame();
        final JTextField day = new JTextField();
        final JTextField month = new JTextField();
        final JTextField year = new JTextField();
        JButton button = new JButton("Submit");
        button.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                date.setDay(Integer.parseInt(day.getText()));
                date.setMonth(Integer.parseInt(month.getText()));
                date.setYear(Integer.parseInt(year.getText()));
            }
        });
        panel.add(new JLabel("Day:"));
        panel.add(day);
        panel.add(new JLabel("Month:"));
        panel.add(month);
        panel.add(new JLabel("Year:"));
        panel.add(year);
        panel.add(button);
        date.parseDayOfWeek();
        return date;
    } 
}