package calendar;

/**
* This class is the framework for a <code>Date</code> type.
* Examples:
*   <code>Date christmas = new Date(25,12,3);</code>
*   <code>Date epoch = new Date(1,1,1970);</code>
*/
public class Date{
    /* This field holds the day of the month */
    private int day;
    /* This field holds the numeric month (Jan - 1, Feb - 2, etc.) */
    private int month;
    /* This field holds the 4 digit year (assume CE/AD) */
    private int year;
    /* This field holds the numeric day of the week (0 - Sunday, 1 - Monday, etc.) */
    private int dayOfWeek;

    /**
    * This is the default constructor which does nothing
    */
    public Date(){}; 

    /**
    * This constructor should be used in almost all circumstances
    *
    * @param day the day to set
    * @param month the month to set
    * @param year the year to set
    */
    public Date(int day, int month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
        int y = year;
        int m = month;
        if(month < 3){
            y--;
        }

        m -= 2;
        if(m <= 0){
            m += 12;
        }
        int c = y / 100;

        this.dayOfWeek = (int)(day + (int)(2.6*m - 0.2) + y % 100 + (y % 100)/4 + c/4 - 2*c) % 7;
    }

    /**
    * Gets the day
    * @return the day
    */
    public int getDay() {
        return day;
    }

    /**
    * Gets the month
    * @return the month
    */
    public int getMonth() {
        return month;
    }

    /**
    * Gets the year
    * @return the year
    */
    public int getYear() {
        return year;
    }

    /**
    * Gets the dayOfWeek
    * @return the dayOfWeek
    */
    public int getDayOfWeek() {
        return dayOfWeek;
    }

    /**
    * Sets the day
    * @param day the day to set
    */
    public void setDay(int day) {
        this.day = day;
    }

    /**
    * Sets the month
    * @param month the month to set
    */
    public void setMonth(int month) {
        this.month = month;
    }

    /**
    * Sets the year
    * @param year the year to set
    */
    public void setYear(int year) {
        this.year = year;
    }

    /**
    * Sets the dayOfWeek
    * @param dayOfWeek the dayOfWeek to set
    */
    public void setDayOfWeek(int dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    /**
    * This method parses the day of week
    * after the day, month, or year of
    * a date is changed. It uses Gauss'
    * day of the week algorithm 
    */
    public void parseDayOfWeek() {
        int y = getYear();
        int m = getMonth();
        if(getMonth() < 3){
            y--;
        }
        m -= 2;
        if(m <= 0){
            m += 12;
        }
        int c = y / 100;

        this.dayOfWeek = (int)(getDay() + (int)(2.6*m - 0.2) + y % 100 + (y % 100)/4 + c/4 - 2*c) % 7;
    }

    /**
    * This method gives a 
    * string representation of
    * the date in the format of
    * "dayOfWeek, (str)month day, year"
    *
    * @return string representation of date
    */
    public String getDate() {
        String dayOfWeek = "";
        String month = "";
        switch(getDayOfWeek()){
            case 0: dayOfWeek = "Sunday"; break;
            case 1: dayOfWeek = "Monday"; break;
            case 2: dayOfWeek = "Tuesday"; break;
            case 3: dayOfWeek = "Wednesday"; break;
            case 4: dayOfWeek = "Thursday"; break;
            case 5: dayOfWeek = "Friday"; break;
            case 6: dayOfWeek = "Saturday"; break;
        }

        switch(getMonth()){
            case 1: month = "January"; break;
            case 2: month = "February"; break;
            case 3: month = "March"; break;
            case 4: month = "April"; break;
            case 5: month = "May"; break;
            case 6: month = "June"; break;
            case 7: month = "July"; break;
            case 8: month = "August"; break;
            case 9: month = "September"; break;
            case 10: month = "October"; break;
            case 11: month = "November"; break;
            case 12: month = "December"; break;
        }

        return dayOfWeek + ", " + month + " " + Integer.toString(getDay()) + ", " + Integer.toString(getYear());
    }
}