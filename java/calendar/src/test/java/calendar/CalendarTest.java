package calendar;

import org.junit.*;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

/**
 * Unit test for simple App.
 */
public class CalendarTest{
    @Test
    public void testCalendar() {
        Date birthday = new Date(5,12,1995);
        assertEquals(birthday.getDay(),5);
        assertEquals(birthday.getMonth(),12);
        assertEquals(birthday.getYear(),1995);
        assertEquals(birthday.getDayOfWeek(),2);
        assertEquals(birthday.getDate(),"Tuesday, December 5, 1995");
    }

    @Test
    public void testTest() {
        assertEquals(1,1);
    }
}
