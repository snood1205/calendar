Public Class Date
    Private _day As Integer
    Private _month As Integer
    Private _year As Integer
    Private _dayOfWeek As Integer

    Public Sub New(ByVal day As Integer, ByVal month As Integer, ByVal year As Integer)
        MyClass._day = day
        MyClass._month = month
        MyClass._year = year
        Dim m As Integer
        Dim y As Integer
        Dim c As Integer
        m = month
        If month < 3 Then
            y = year - 1
        Else
            y = year
        End If
        m = m - 2
        If m <= 0 Then
            m = m + 12
        End If
        c = year / 100
        MyClass._dayOfWeek = CInt(day + CInt(2.6*m - 0.2) + (y mod 100) + (y mod 100)/4 + c/4  + 2*c) mod 7
    End Sub

    Property Day() As Integer
        Get
            return _day
        End Get
        Set(ByVal day As Integer)
            _day = day
        End Set
    End Property

    Property Month() As Integer
        Get
            return _month
        End Get
        Set(ByVal month As Integer)
            _month = month
        End Set
    End Property

    Property Year() As Integer
        Get
            return _year
        End Get
        Set(ByVal year As Integer)
            _year = year
        End Set
    End Property

    Property DayOfWeek() As Integer
        Get
            return _dayOfWeek
        End Get
        Set(ByVal dayOfWeek As Integer)
            _dayOfWeek = dayOfWeek
        End Set
    End Property

    Public Function Date() As String
        Dim dow As String
        Dim mon As String
        If DayOfWeek() = 0 Then
            dow = "Sunday"
        Elseif DayOfWeek() = 1 Then
            dow = "Monday"
        Elseif DayOfWeek() = 2 Then
            dow = "Tuesday"
        Elseif DayOfWeek() = 3 Then
            dow = "Wednesday"
        Elseif DayOfWeek() = 4 Then
            dow = "Thursday"
        Elseif DayOfWeek() = 5 Then
            dow = "Friday"
        Elseif DayOfWeek() = 6 Then
            dow = "Saturday"
        End If

        If Month() = 1 Then
            mon =  "January"
        Elseif Month() = 2 Then
            mon =  "February"
        Elseif Month() = 3 Then
            mon =  "March"
        Elseif Month() = 4 Then
            mon =  "April"
        Elseif Month() = 5 Then
            mon =  "May"
        Elseif Month() = 6 Then
            mon =  "June"
        Elseif Month() = 7 Then
            mon =  "July"
        Elseif Month() = 8 Then
            mon =  "August"
        Elseif Month() = 9 Then
            mon =  "September"
        Elseif Month() = 10 Then
            mon =  "October"
        Elseif Month() = 11 Then
            mon =  "November"
        Elseif Month() = 12 Then
            mon =  "December"
        End If

        Return dow & ", " & mon & " " & Cstr(Day()) & ", " & Cstr(Year())
    End Function
End Class
