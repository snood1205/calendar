# coding: utf-8
lib = File.expand_path('../lib',__FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include? lib

Gem::Specification new do |spec|
	spec.name		= "calendar"
	spec.version		= '1.0'
	spec.authors		= ["Eli Sadoff"]
	spec.email		= ["snood1205@gmail.com"]
	spec.summary		= %q{A simple calendar}
	spec.description	= spec.summary
	spec.homepage		= "https://github.com/snood1205/calendar"
	spec.license		= "MIT"
	spec.files		= ['lib/calendar.rb']
	spec.executables	= ['bin/calendar']
	spec.test_files		= ['tests/test_calendar.rb']
	spec.require_paths	= ["lib"]
end
