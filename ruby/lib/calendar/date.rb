class Date
    attr_accessor :day, :month, :year, :day_of_week

    def initialize day, month, year
        @day = day
        @month = month
        @year = year
        y = year
        m = month
        y -= 1 if month < 3
        m -= 2
        m += 12 if m <= 0
        c = y / 100
        @day_of_week = (day + (2.6 * m - 0.2).to_i + y % 100 + (y % 100)/4 + c/4 - 2 * c).to_i % 7
    end

    def date
        dow = case @day_of_week
        when 0; "Sunday"
        when 1; "Monday"
        when 2; "Tuesday"
        when 3; "Wednesday"
        when 4; "Thursday"
        when 5; "Friday"
        when 6; "Saturday"
        end

        mon = case @month
        when 1; "January"
        when 2; "February"
        when 3; "March"
        when 4; "April"
        when 5; "May"
        when 6; "June"
        when 7; "July"
        when 8; "August"
        when 9; "September"
        when 10; "October"
        when 11; "November"
        when 12; "December"
        end

        return "#{dow}, #{mon} #{@day}, #{@year}"
    end
end