require "./lib/calendar.rb"
require "test/unit"

class TestCalendar < Test::Unit::TestCase

	def test_sample
        bd = Date.new(5,12,1995)
        today = Date.new(25,5,2016)
		assert_equal(bd.date, "Tuesday, December 5, 1995")
        assert_equal(today.date, "Wednesday, May 25, 2016")
	end
end
