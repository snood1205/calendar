from setuptools import setup, find_packages
from codecs import open
from os import path

here = path.abspath(path.dirname(__file__))

with open(path.join(here, '../README.md'), encoding='utf-8') as f:
	long_description = f.read()

setup(
	name='calendar',
	version='1.0.0',
	description='A sample calendar',
	long_descrption=long_description,
	url='https://github.com/snood1205/calendar',
	author='Eli Sadoff',
	author_email='snood1205@gmail.com',
	license='MIT',
	classifiers=[
		'Development Status :: 2 - Pre-Alpha',
		'Intended Audience :: Developers',
		'Topic :: Test :: Test',
		'License :: OSI Approved :: MIT License',
		'Programming Language :: Python :: 2',
		'Programming Language :: Python :: 3',
		],
	keywords='calendar',
	packages=find_packages(exclude=['contrib','docs','tests']),
	install_requires=['peppercorn'],
	entry_points={
		'console_scripts': [
			'calendar=calendar:main',
			],
		},
	)
