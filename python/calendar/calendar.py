class Date:
    def __init__(self,day,month,year):
        self.day = day
        self.month = month
        self.year = year
        y = year
        m = month
        if month < 3: y -= 1 
        m -= 2
        if m <= 0: m += 12
        c = y / 100
        self.day_of_week = int(day + int(2.6 * m - 0.2)+ y % 100 + (y % 100)/4 + c/4 - 2 * c) % 7

    def date(self):
        if self.day_of_week == 0:
            dow = "Sunday"
        elif self.day_of_week == 1:
            dow = "Monday"
        elif self.day_of_week == 2:
            dow = "Tuesday"
        elif self.day_of_week == 3:
            dow = "Wednesday"
        elif self.day_of_week == 4:
            dow = "Thursday"
        elif self.day_of_week == 5:
            dow = "Friday"
        else:
            dow = "Saturday"

        if self.month == 1:
            mon =  "January"
        elif self.month == 2:
            mon =  "February"
        elif self.month == 3:
            mon =  "March"
        elif self.month == 4:
            mon =  "April"
        elif self.month == 5:
            mon =  "May"
        elif self.month == 6:
            mon =  "June"
        elif self.month == 7:
            mon =  "July"
        elif self.month == 8:
            mon =  "August"
        elif self.month == 9:
            mon =  "September"
        elif self.month == 10:
            mon =  "October"
        elif self.month == 11:
            mon =  "November"
        elif self.month == 12:
            mon =  "December"

        return dow + ", " + mon + " " + str(self.day) + ", " + str(self.year)

def main():
    bd = Date(5,12,1995)
    print(bd.date())

main()

