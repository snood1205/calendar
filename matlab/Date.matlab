classdef Date
    properties
        Day
        Month
        Year
        DayOfWeek
    end
    methods
        function obj = Date(d, m, y)
            obj.Day = d;
            obj.Month = m;
            obj.Year = y;
            if(m < 3)
                y = y - 1;
            end
            m = m - 2;
            if(m <= 0)
                m = m + 12;
            end
            c = idivide(y,100,'floor');
            ymod = mod(y,100);
            obj.DayOfWeek = mod(d+floor(2.6*m-0.2)+ymod+idivide(ymod,4,'floor')+idivide(c,4,'floor')-2*c,7);
        end
        function date = getDate(obj)
            switch obj.DayOfWeek
                case 0
                    dow = 'Sunday';
                case 1
                    dow = 'Monday';
                case 2
                    dow = 'Tuesday';
                case 3
                    dow = 'Wednesday';
                case 4
                    dow = 'Thursday';
                case 5
                    dow = 'Friday';
                case 6
                    dow = 'Saturday';
            end

            switch obj.Month
                case 1
                    mon = 'January';
                case 2
                    mon = 'February';
                case 3
                    mon = 'March';
                case 4
                    mon = 'April';
                case 5
                    mon = 'May';
                case 6
                    mon = 'June';
                case 7
                    mon = 'July';
                case 8
                    mon = 'August';
                case 9
                    mon = 'September';
                case 10
                    mon = 'October';
                case 11
                    mon = 'November';
                case 12
                    mon = 'December';
            end
            date = strcat(dow,', ',mon,' ',int2str(obj.Day),', ',int2str(obj.Year));
        end
    end
end