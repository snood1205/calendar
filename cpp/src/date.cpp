#include "date.hpp"

Date :: Date(int d, int m, int y) {
    day = d;
    month = m;
    year = y;
    if(m < 3)
        y--;
    m -= 2;
    if(m <= 0)
        m += 12;
    int c = y / 100;

    dayOfWeek = (int)(d + floor(2.6*m - 0.2) + y % 100 + (y % 100)/4 + c/4 - 2*c) % 7;
}

int Date :: getDay() {
    return day;
}

int Date :: getMonth() {
    return month;
}

int Date :: getYear() {
    return year;
}

int Date :: getDayOfWeek() {
    return dayOfWeek;
}

void Date :: setDay(int d) {
    day = d;
}

void Date :: setMonth(int m) {
    month = m;
}

void Date :: setYear(int y) {
    year = y;
}

void Date :: setDayOfWeek(int d) {
    dayOfWeek = d;
}

std::string Date :: getDate() {
    std::string dow;
    std::string mon;
    switch(getDayOfWeek()){
        case 0:
            dow = "Sunday";
            break;
        case 1:
            dow = "Monday";
            break;
        case 2:
            dow = "Tuesday";
            break;
        case 3:
            dow = "Wednesday";
            break;
        case 4:
            dow = "Thursday";
            break;
        case 5:
            dow = "Friday";
            break;
        case 6:
            dow = "Saturday";
            break;    
    }

    switch(getMonth()){
        case 1:
            mon = "January";
            break;
        case 2:
            mon = "February";
            break;
        case 3:
            mon = "March";
            break;
        case 4:
            mon = "April";
            break;
        case 5:
            mon = "May";
            break;
        case 6:
            mon = "June";
            break;
        case 7:
            mon = "July";
            break;
        case 8:
            mon = "August";
            break;
        case 9:
            mon = "September";
            break;
        case 10:
            mon = "October";
            break;
        case 11:
            mon = "November";
            break;
        case 12:
            mon = "December";
            break;
    }
    return dow + ", " + mon + " " + std::to_string(getDay()) + ", " + std::to_string(getYear());
}