#include "test.hpp"

int main(int argc, char * argv[]) {
    return 0;
}

void Test :: userInput() {
    int d;
    int m;
    int y;
    std::cout << "Enter the day:" << std::endl;
    std::cin >> d;
    std::cout << "Enter the month:" << std::endl;
    std::cin >> m;
    std::cout << "Enter the year:" << std::endl;
    std::cin >> y;
    Date birthday(d,m,y);
    std::cout << birthday.getDate() << std::endl;
}