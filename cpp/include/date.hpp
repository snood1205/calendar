#ifndef DATE_HPP_
#define DATE_HPP_

#include <string>
#include <cmath>

class Date {
private:
    int day;
    int month;
    int year;
    int dayOfWeek;
public:
    Date(int d, int m, int y);
    ~Date(){}
    int getDay();
    int getMonth();
    int getYear();
    int getDayOfWeek();
    void setDay(int d);
    void setMonth(int m);
    void setYear(int y);
    void setDayOfWeek(int d);
    std::string getDate();
};

#endif